# geopagos-test

## Application test de geopagos.

## SPEC:

MODELADO DE CLASES
  Desarrollar una clase que permita la creación de objetos que representen figuras
  geométricas dadas (cuadrado, triangulo, círculo) proveyendo un parámetro de entrada
  único que identifiqué el tipo de objeto esperado como retorno. Tip de ayuda: “Factory”
  La estructura del modelo de clases que deben tener las implementaciones de dichas
  figuras geométricas será la siguiente:
    - Se debe definir una interfaz que exija la definición de métodos que permitan la obtención de los datos: superficie; base; altura; diámetro; tipo de figura geométrica.
    - Puede definirse alguna clase intermedia (no instanciable) para reducir la redundancia de código.
    - Cada una de sus concreciones debe, ante la invocación de alguno de sus métodos, retornar el valor correspondiente o null en caso de que la figura geométrica correspondiente no posea dicha característica.
    - En caso de conocer la notación se valorará el diseño de su diagrama UML.

SERVICIOS
  Desarrollar una API Rest en JAVA, para el registro y obtención de datos de figuras
  geométricas (cuadrado, triángulo, círculo) y sus distintos parámetros (superficie; base;
  altura; diámetro; tipo de figura geométrica).
    - Las librerías/frameworks a utilizar para esta prueba se deja a elección del aspirante.
    - Se considerará el uso de inyección de dependencias.


## Requisitos y setup

Para poder correr esta applicacion se debera contar con
  Acceso a internet para descargar dependencias.
  Java8.
  Maven3.
  

  
Una vez descargado el proyecto usar los siguientes comandos para ejecutarlo
  mvn clean install 
  mvn package
  
  
Para iniciar el servicio con una base en memoria ejecutar:
  java -jar .\target\geopagos-test-1.0.0-SNAPSHOT.jar --spring.config.location=file:./target/classes/test-application.properties --logging.config=./target/classes/logback-prod.xml
  
En el caso de contar con una base de datos editar el archivo "application.properties" ubicado en el directorio "src/main/resources" y luego ejecutar
  mvn clean install
  mvn package
  java -jar .\target\geopagos-test-1.0.0-SNAPSHOT.jar --spring.config.location=file:./target/classes/application.properties --logging.config=./target/classes/logback-prod.xml


Las documentacion de la implementacion REST se encuenta en:
  http://localhost:8080/swagger-ui.html
  
Para el estado de la applicacion ingresar e:

  http://localhost:8080/ping
  
## Documentacion del proyecto.

Se encuentra disponible la documentacion de clases generadas por javadoc y code coverage.

  para disponibilizar javadoc.
    - mvn clean install
    - La documentacion quedara guardada en ./target/apidoc/index.html
