package com.fvittorini.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fvittorini.application.Application;
import com.fvittorini.domain.PolygonFactory;
import com.fvittorini.domain.PolygonRepository;
import com.fvittorini.domain.entity.Polygon;
import com.fvittorini.swagger.model.PolygonCreationDTO;
import com.fvittorini.swagger.model.PolygonDTO;
import com.fvittorini.swagger.model.PolygonTypeDTO;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:test-application.properties")
@SpringBootTest(classes = {Application.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PolygonServiceIntegrationTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  private PolygonRepository polygonRepository;
  private PolygonService polygonService;
  private EntityManager entityManager;

  @Autowired
  private ApplicationContext context;

  @Before
  public void setUp() {
    polygonService = context.getBean(PolygonService.class);
    polygonRepository = context.getBean(PolygonRepository.class);
    entityManager = context.getBean(EntityManager.class);
  }

  @Test
  public void testCreatePolygon() throws Exception {
    String polygonName = "circle";
    PolygonTypeDTO polygonTypeDTO = PolygonTypeDTO.CIRCLE;
    PolygonCreationDTO polygonCreationDTO = buildPolygonCreationDTO(polygonName,
      polygonTypeDTO, 1);
    PolygonDTO resultCreation = polygonService.createPolygon(polygonCreationDTO);
    assertThat(resultCreation.getId(), is(not(nullValue())));
    PolygonDTO result = polygonService.getPolygonById(resultCreation.getId());
    assertThat(result, is(not(nullValue())));
  }

  @Test
  public void testDeletePolygon() throws Exception {
    String polygonName = "triangle";
    PolygonTypeDTO polygonTypeDTO = PolygonTypeDTO.TRIANGLE;
    PolygonCreationDTO polygonCreationDTO = buildPolygonCreationDTO(polygonName,
      polygonTypeDTO, 3);
    Polygon p = PolygonFactory.createPolygon(polygonCreationDTO);
    polygonRepository.persist(p);
    Long polygonId = p.getId();
    PolygonDTO result = polygonService.getPolygonById(polygonId);
    assertThat(result, is(not(nullValue())));

    polygonService.deletePolygon(polygonId);
    thrown.expect(RuntimeException.class);
    thrown.expectMessage("Polygon not found");
    polygonService.getPolygonById(polygonId);
  }

  @Test
  public void testFindPolygonByID_notFound() throws Exception {
    Long polygonId = 1L;
    thrown.expect(RuntimeException.class);
    thrown.expectMessage("Polygon not found");
    polygonService.getPolygonById(polygonId);
  }

  @Test
  public void testFindPolygons_noFilter() throws Exception {
    Long polygonId = 1L;
    String polygonName = "triangle";
    PolygonTypeDTO polygonTypeDTO = PolygonTypeDTO.TRIANGLE;
    PolygonCreationDTO polygonCreationDTO = buildPolygonCreationDTO(polygonName,
      polygonTypeDTO, 3);
    Polygon p = PolygonFactory.createPolygon(polygonCreationDTO);


    String polygonName2 = "triangle";
    PolygonTypeDTO polygonTypeDTO2 = PolygonTypeDTO.TRIANGLE;
    PolygonCreationDTO polygonCreationDTO2 = buildPolygonCreationDTO(polygonName2,
      polygonTypeDTO2, 3);
    Polygon p2 = PolygonFactory.createPolygon(polygonCreationDTO2);

    String polygonName3 = "rectangre";
    PolygonTypeDTO polygonTypeDTO3 = PolygonTypeDTO.RECTANGLE;
    PolygonCreationDTO polygonCreationDTO3 = buildPolygonCreationDTO(polygonName3,
      polygonTypeDTO3, 2);
    Polygon p3 = PolygonFactory.createPolygon(polygonCreationDTO3);

    polygonRepository.persist(p);
    polygonRepository.persist(p2);
    polygonRepository.persist(p3);

    List<PolygonDTO> listPolygons = polygonService.listPolygons(null);
    assertThat(listPolygons.size(), equalTo(3));
  }

  @Test
  public void testFindPolygons_typeFilter() throws Exception {
    Long polygonId = 1L;
    String polygonName = "triangle";
    PolygonTypeDTO polygonTypeDTO = PolygonTypeDTO.TRIANGLE;
    PolygonCreationDTO polygonCreationDTO = buildPolygonCreationDTO(polygonName,
      polygonTypeDTO, 3);
    Polygon p = PolygonFactory.createPolygon(polygonCreationDTO);


    String polygonName2 = "triangle";
    PolygonTypeDTO polygonTypeDTO2 = PolygonTypeDTO.TRIANGLE;
    PolygonCreationDTO polygonCreationDTO2 = buildPolygonCreationDTO(polygonName2,
      polygonTypeDTO2, 3);
    Polygon p2 = PolygonFactory.createPolygon(polygonCreationDTO2);

    String polygonName3 = "rectangle";
    PolygonTypeDTO polygonTypeDTO3 = PolygonTypeDTO.RECTANGLE;
    PolygonCreationDTO polygonCreationDTO3 = buildPolygonCreationDTO(polygonName3,
      polygonTypeDTO3, 2);
    Polygon p3 = PolygonFactory.createPolygon(polygonCreationDTO3);

    polygonRepository.persist(p);
    polygonRepository.persist(p2);
    polygonRepository.persist(p3);

    List<PolygonDTO> listPolygons = polygonService.listPolygons(PolygonTypeDTO.RECTANGLE.name());
    assertThat(listPolygons.size(), equalTo(1));
    for (PolygonDTO polygonDTO : listPolygons){
      assertThat(polygonDTO.getType(), is(PolygonTypeDTO.RECTANGLE));
    }
  }

  private PolygonCreationDTO buildPolygonCreationDTO(final String polygonName,
                                                     final PolygonTypeDTO polygonTypeDTO,
                                                     final Integer sides){

    List<BigDecimal> sideLength = new ArrayList<>();
    for (int i = 0; i < sides; i++) {
      sideLength.add(BigDecimal.TEN);
    }
    PolygonCreationDTO polygonCreationDTO = new PolygonCreationDTO();
    polygonCreationDTO.setName(polygonName);
    polygonCreationDTO.setType(polygonTypeDTO);
    polygonCreationDTO.setSideLength(sideLength);
    return polygonCreationDTO;
  }
}