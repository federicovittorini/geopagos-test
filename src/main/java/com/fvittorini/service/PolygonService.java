package com.fvittorini.service;

import java.util.ArrayList;
import java.util.List;

import com.fvittorini.domain.PolygonFactory;
import com.fvittorini.domain.PolygonRepository;
import com.fvittorini.domain.PolygonType;
import com.fvittorini.domain.entity.Polygon;
import com.fvittorini.swagger.model.PolygonCreationDTO;
import com.fvittorini.swagger.model.PolygonDTO;
import org.apache.commons.lang3.Validate;

/**
 * The polygon services class.
 * This class provide capabilities to manipulate an create polygons.
 * @author fedevittorini
 */
public class PolygonService {

  /** The polygon repository. It cannot be null.
   */
  private PolygonRepository polygonRepository;

  /** Construct a full functional polygon service.
   *
   * @param thePolygonRepository The polygon repository. It cannot be null.
   */
  public PolygonService(final PolygonRepository thePolygonRepository){
    Validate.notNull(thePolygonRepository, "The poligon repository cannot be null");
    this.polygonRepository = thePolygonRepository;
  }

  /** Attempt to create a polygon.
   *
   * @param thePolygonCreationDTO the polygon creation dto. It cannot be null.
   * @return the full polygon DTO. Never null.
   */
  public PolygonDTO createPolygon(final PolygonCreationDTO thePolygonCreationDTO) {
    Validate.notNull(thePolygonCreationDTO, "The polygon creation DTO cannot be null");
    Polygon polygon = PolygonFactory.createPolygon(thePolygonCreationDTO);
    polygonRepository.persist(polygon);
    PolygonDTO polygonDTO = PolygonFactory.fillFullPolygonDTO(polygon);

    return polygonDTO;
  }

  /** Attempt to delete a polygon by id.
   *
   * @param polygonId the polygon id. It cannot be null.
   */
  public void deletePolygon(final Long polygonId) {
    Validate.notNull(polygonId, "The polygon id cannot be null.");
    polygonRepository.delete(polygonId);
  }

  /** Attempt to retrieve a polygon by id.
   *
   * @param polygonId the polygon id to look for. It cannot be null.
   * @return the full polygon DTO. Never null.
   */
  public PolygonDTO getPolygonById(final Long polygonId) {
    Validate.notNull(polygonId, "The polygon id cannot be null.");
    Polygon polygon = polygonRepository.getById(polygonId);
    PolygonDTO polygonDTO = PolygonFactory.fillFullPolygonDTO(polygon);
    return polygonDTO;
  }

  /** Attempt to retrieve a list of available polygons.
   *
   * @param typeFilter the polygon type to filter. It may be null.
   * @return the list of polygons. Never null.
   */
  public List<PolygonDTO> listPolygons(final String typeFilter) {
    PolygonType type = null;
    if (typeFilter != null && !typeFilter.equals("")) {
      type = PolygonType.valueOf(typeFilter);
    }
    List<Polygon> polygonList = polygonRepository.listWithFilters(type);
    List<PolygonDTO> polygonDTOList = new ArrayList<>();
    for (Polygon polygon : polygonList) {
      PolygonDTO polygonDTO = PolygonFactory.fillFullPolygonDTO(polygon);
      polygonDTOList.add(polygonDTO);
    }
    return polygonDTOList;
  }

}
