package com.fvittorini.config;

import javax.persistence.EntityManagerFactory;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The Hibernate configuration.
 */
@EnableTransactionManagement
@EntityScan(basePackages={"com.fvittorini.domain.entity"})
@Configuration
public class HibernateConfig {

  /** The transaction manager.
   *
   * @param entityManagerFactory the entity manager factory. It cannot be null.
   * @return the transaction manager. Never null.
   */
  @Bean
  public PlatformTransactionManager getTransactionManager(
    final EntityManagerFactory entityManagerFactory) {
    return new JpaTransactionManager(entityManagerFactory);
  }

}