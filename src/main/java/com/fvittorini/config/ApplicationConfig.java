package com.fvittorini.config;

import javax.persistence.EntityManager;

import com.fvittorini.controller.GeometryApiRestController;
import com.fvittorini.controller.HealthCheckApiRestController;
import com.fvittorini.domain.PolygonRepository;
import com.fvittorini.service.PolygonService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/** The main application bean definition.
 *
 */
@Configuration
@EnableSwagger2
@EnableTransactionManagement
@Import({HibernateConfig.class})
public class ApplicationConfig {

  /** The health check API REST controller bean.
   *
   * @return The health check API REST controller. Never null.
   */
  @Bean
  public HealthCheckApiRestController getHealthCheckApiRestController(){
    return new HealthCheckApiRestController();
  }

  /** The polygon repository bean.
   *
   * @param theEntityManager the entity manger required to interact with the DB.
   *                         It cannot be null.
   * @return the polygon repository. Never null.
   */
  @Bean
  public PolygonRepository getPolygonRepository(final EntityManager theEntityManager) {
    return new PolygonRepository(theEntityManager);
  }

  /** The polygon service bean.
   *
   * @param thePolygonRepository the polygon repository to persist and retrieve data.
   *                             It cannot be null.
   * @return the polygon service.
   */
  @Bean
  public PolygonService getPolygonService(final PolygonRepository thePolygonRepository) {
    return new PolygonService(thePolygonRepository);
  }

  /** The geometry API REST controller bean.
   *
   * @return The geometry API REST controller. Never null.
   * @param thePolygonService the polygon service. It cannot be null
   */
  @Bean
  public GeometryApiRestController getGeometryApiRestController(
    final PolygonService thePolygonService){
    return new GeometryApiRestController(thePolygonService);
  }

  /** The Docket bean.
   *
   * @return the docket bean. Never null.
   */
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.basePackage("com.fvittorini.swagger"))
      .paths(PathSelectors.any())
      .build();
  }

}
