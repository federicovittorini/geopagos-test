package com.fvittorini.controller;

import com.fvittorini.swagger.api.PingApiDelegate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * HealthCheck API Rest controller.
 */
public class HealthCheckApiRestController implements PingApiDelegate {

  /** HealthCheck API rest controller constructor.
   */
  public HealthCheckApiRestController() {}

  /** Perform a health check and return pong.
   *
   * @return the response. Never null.
   */
  public ResponseEntity<String> healthCheck() {
    return new ResponseEntity<>("pong", HttpStatus.OK);
  }

}
