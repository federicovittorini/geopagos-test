package com.fvittorini.controller;

import java.util.List;

import com.fvittorini.service.PolygonService;
import com.fvittorini.swagger.api.GeometryApiDelegate;
import com.fvittorini.swagger.model.PolygonCreationDTO;
import com.fvittorini.swagger.model.PolygonDTO;
import com.fvittorini.swagger.model.PolygonListDTO;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Geometry API Rest controller.
 */
public class GeometryApiRestController implements GeometryApiDelegate {

  /**
   * The class logger. It cannot be null.
   */
  private final Logger logger = LoggerFactory.getLogger(GeometryApiRestController.class);

  /** The polygon service. It cannot be null.
   */
  private PolygonService polygonService;

  /** Geometry API rest controller constructor.
   *
   * @param thePolygonService the polygon service. It cannot be null.
   */
  public GeometryApiRestController(final PolygonService thePolygonService) {
    Validate.notNull(thePolygonService, "The polygon services cannot be null");
    this.polygonService = thePolygonService;
  }

  /** Attempt to create a polygon.
   *
   * @param thePolygonCreationDTO the polygon creation dto. It cannot be nul.
   * @return the response. Never null.
   */
  public ResponseEntity<PolygonDTO> createPolygon(
    final PolygonCreationDTO thePolygonCreationDTO) {
    Validate.notNull(thePolygonCreationDTO, "The polygon creation dto cannot be nll");
    PolygonDTO polygonDTO;
    try {
      polygonDTO = polygonService.createPolygon(thePolygonCreationDTO);
    } catch (Exception e) {
      logger.error("error trying to create polygon ", e);
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(polygonDTO, HttpStatus.OK);
  }

  /** Attempt to delete a polygon by given id.
   *
   * @param polygonId the polygon id. It cannot be null
   * @return the response. Never null.
   */
  public ResponseEntity<Void> deleteById(final Long polygonId) {
    Validate.notNull(polygonId, "The polygon id cannot be nll");
    try {
      polygonService.deletePolygon(polygonId);
    } catch (Exception e) {
      logger.error("error trying to delete by id ", e);
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /** Attempt to retrieve a polygon by given id.
   *
   * @param polygonId the polygon id. It cannot be null
   * @return the response. Never null.
   */
  public ResponseEntity<PolygonDTO> getById(final Long polygonId) {
    Validate.notNull(polygonId, "The polygon id cannot be nll");
    PolygonDTO polygonDTO;
    try {
      polygonDTO = polygonService.getPolygonById(polygonId);
    } catch (Exception e) {
      logger.error("error trying to get by id ", e);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(polygonDTO, HttpStatus.OK);
  }

  /** List all polygons filtered by params.
   *
   * @param type the polygon type filter param. It may be null.
   * @return the response. Never null.
   */
  public ResponseEntity<PolygonListDTO> listPolygons(final String type) {
    PolygonListDTO polygonListDTO = new PolygonListDTO();
    try {
      List<PolygonDTO> polygonDTOList = polygonService.listPolygons(type);
      polygonListDTO.addAll(polygonDTOList);
    } catch (Exception e) {
      logger.error("error trying to list polygons ", e);
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(polygonListDTO, HttpStatus.OK);
  }

}
