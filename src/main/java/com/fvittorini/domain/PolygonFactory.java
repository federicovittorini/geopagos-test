package com.fvittorini.domain;

import com.fvittorini.domain.entity.Circle;
import com.fvittorini.domain.entity.Polygon;
import com.fvittorini.domain.entity.Rectangle;
import com.fvittorini.domain.entity.Triangle;
import com.fvittorini.swagger.model.PolygonCreationDTO;
import com.fvittorini.swagger.model.PolygonDTO;
import com.fvittorini.swagger.model.PolygonTypeDTO;
import org.apache.commons.lang3.Validate;

/** Factory class to build polygons.
 *
 * @author fedevittorini
 */
public class PolygonFactory {

  /** Polygon factory to create a polygon by given polygonDTO.
   *
   * @param polygonDTO the polygon creation DTO. It cannot be null.
   * @return the polygon. Never null
   */
  public static Polygon createPolygon(final PolygonCreationDTO polygonDTO) {
    Validate.notNull(polygonDTO, "The polygon DTO cannot be null");
    Polygon polygon;
    switch (polygonDTO.getType()) {
      case TRIANGLE:
        polygon = new Triangle(polygonDTO.getName(), polygonDTO.getSideLength());
        break;
      case RECTANGLE:
        polygon = new Rectangle(polygonDTO.getName(), polygonDTO.getSideLength());
        break;
      case CIRCLE:
        polygon = new Circle(polygonDTO.getName(), polygonDTO.getSideLength());
        break;
      default:
        throw new RuntimeException("Unsupported polygon type " + polygonDTO.getType());
    }
    return polygon;
  }

  /** Fill full polygon dto based on polygon.
   *
   * @param polygon the polygon. It cannot be null.
   * @return the full polygon dto. Never null.
   */
  public static PolygonDTO fillFullPolygonDTO(final Polygon polygon){
    PolygonType type = polygon.getType();
    PolygonDTO fullPolygonDTO = new PolygonDTO();
    fullPolygonDTO.setId(polygon.getId());
    fullPolygonDTO.setName(polygon.getName());
    fullPolygonDTO.setType(PolygonTypeDTO.valueOf(type.getName()));
    fullPolygonDTO.setSideLength(polygon.getSideLength());
    fullPolygonDTO.setPerimeter(polygon.getPerimeter());
    fullPolygonDTO.setArea(polygon.getArea());
    fullPolygonDTO.setWidth(polygon.getWidth());
    fullPolygonDTO.setHeight(polygon.getHeight());
    fullPolygonDTO.setRadius(polygon.getRadius());
    fullPolygonDTO.setHypotenuse(polygon.getHypotenuse());
    fullPolygonDTO.setAdjacent(polygon.getAdjacent());
    fullPolygonDTO.setOpposite(polygon.getOpposite());

    return fullPolygonDTO;
  }
}
