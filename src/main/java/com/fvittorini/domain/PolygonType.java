package com.fvittorini.domain;

import org.apache.commons.lang3.Validate;

/** PolygonType enum.
 * This enum to label different types of available polygons.
 * @author fedevittorini
 */
public enum PolygonType {
  /**
   * The triangle polygon type.
   * This type mean that the polygon should have only 3 sides.
   */
  TRIANGLE(VALUES.TRIANGLE, 3),
  /**
   * The rectangle polygon type.
   * This type mean that the polygon should have only 2 sides, width and height.
   */
  RECTANGLE(VALUES.RECTANGLE, 2),
  /**
   * The rectangle polygon type.
   * This type mean that the polygon should have infinite sides.
   */
  CIRCLE(VALUES.CIRCLE, 1);

  /**
   * The polygon sides. It cannot be null.
   */
  private Integer sides;

  /**
   * The polygon name. It cannot be null.
   */
  private String name;

  /** Polygon type constructor.
   *
   * @param theName the polygon name. It cannot be null.
   * @param theSides the polygon sides. It cannot be null.
   */
  PolygonType(final String theName, final Integer theSides) {
    Validate.notNull(theName, "The name cannot be null.");
    Validate.notNull(theSides, "The sides cannot be null.");
    this.sides = theSides;
    this.name = theName;
  }

  /** The polygon sides.
   * if sides is 1 and its a CIRCLE then it has infinite sides.
   * @return the sides of the polygon. Never null.
   */
  public Integer getSides() {
    return this.sides;
  }

  /** The polygon name.
   *
   * @return the polygon name. Never null.
   */
  public String getName() {
    return this.name;
  }

  /** Inner values class to represent enum as text.
   *
   */
  public static class VALUES {
    /**
     * Triangle polygon type constant name.
     */
    public static final String TRIANGLE = "TRIANGLE";
    /**
     * Rectangle polygon type constant name.
     */
    public static final String RECTANGLE = "RECTANGLE";
    /**
     * circle polygon type constant name.
     */
    public static final String CIRCLE = "CIRCLE";
  }

}
