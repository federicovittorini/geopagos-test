package com.fvittorini.domain.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.List;

import com.fvittorini.domain.PolygonType;
import org.apache.commons.lang3.Validate;

/**
 * Rectangle Polygon domain class.
 * This class is meant to represent a rectangle based on its sides size.
 * @author fedevittorini
 */
@Entity
@DiscriminatorValue(PolygonType.VALUES.RECTANGLE)
public class Rectangle extends Polygon {

  /** Construct an empty polygon.
   * Just for Hibernate.
   */
  public Rectangle() {}

  /** Construct a full new quadrilateral polygon.
   * The polygon sides should only have 2 entries.
   *
   * @param theName the polygon name. It cannot be null.
   * @param theSidesLength the polygon sides length. It cannot be empty.
   */
  public Rectangle(final String theName, final List<BigDecimal> theSidesLength) {
    super(theName, PolygonType.RECTANGLE, theSidesLength);
  }

  /** The polygon perimeter.
   *
   * @return the polygon perimeter. Never null.
   */
  @Override
  public BigDecimal getPerimeter() {
    BigDecimal perimeter = BigDecimal.ZERO;
    List<BigDecimal> sides = getSideLength();
    PolygonType polygonType = this.getType();
    for (int i = 0; i < polygonType.getSides(); i++) {
      perimeter = perimeter.add(sides.get(i));
    }

    return perimeter.multiply(BIG_DECIMAL_2).setScale(2, BigDecimal.ROUND_FLOOR);
  }

  /** The polygon area.
   *
   * @return the polygon area. Never null.
   */
  @Override
  public BigDecimal getArea() {
    BigDecimal area;
    area = getWidth().multiply(getHeight());
    return area.setScale(2, BigDecimal.ROUND_FLOOR);
  }

  /** The rectangle width.
   * if there is no side configured, then 0 will be returned.
   *
   * @return the width, Never null.
   */
  public BigDecimal getWidth() {
    BigDecimal width = BigDecimal.ZERO;
    if (!this.getSideLength().isEmpty()) {
      width = this.getSideLength().get(0);
    }
    return width;
  }

  /** The rectangle height.
   * if there is no side configured, then 0 will be returned.
   *
   * @return the height, Never null.
   */
  public BigDecimal getHeight() {
    BigDecimal height = BigDecimal.ZERO;
    if (!this.getSideLength().isEmpty() && this.getSideLength().size() >= 2) {
      height = this.getSideLength().get(1);
    }
    return height;
  }

  /**
   * The polygon radius.
   *
   * @return the polygon radius. It may be null.
   */
  @Override
  public BigDecimal getRadius() {
    return null;
  }

  /**
   * The polygon triangle hypotenuse side.
   *
   * @return the polygon triangle hypotenuse side. It may be null.
   */
  @Override
  public BigDecimal getHypotenuse() {
    return null;
  }

  /**
   * The polygon triangle adjacent side.
   *
   * @return the polygon triangle adjacent side. It may be null.
   */
  @Override
  public BigDecimal getAdjacent() {
    return null;
  }

  /**
   * The polygon triangle opposite side.
   *
   * @return the polygon triangle opposite side. It may be null.
   */
  @Override
  public BigDecimal getOpposite() {
    return null;
  }

  /** Verify the values are valid and the shape can be constructed.
   *
   */
  public void verifyValues() {
    Validate.isTrue(getWidth().compareTo(BigDecimal.ZERO) > 0,
      "The width cannot be ZERO or negative");
    Validate.isTrue(getHeight().compareTo(BigDecimal.ZERO) > 0,
      "The height cannot be ZERO or negative");
  }
}
