package com.fvittorini.domain.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.List;

import com.fvittorini.domain.PolygonType;
import org.apache.commons.lang3.Validate;

/**
 * Polygon domain class.
 * This class is meant to represent a triangle based on its sides size.
 * @author fedevittorini
 */
@Entity
@DiscriminatorValue(PolygonType.VALUES.TRIANGLE)
public class Triangle extends Polygon {

  /**
   * Hypotenuse side number.
   */
  private static final int HYPOTENUSE_SIDE = 1;
  /**
   * Adjacent side number.
   */
  private static final int ADJACTENT_SIDE = 2;
  /**
   * Opposite side number.
   */
  private static final int OPPOSITE_SIDE = 3;

  /** Construct an empty triangle polygon.
   * Just for Hibernate.
   */
  public Triangle() {}

  /** Construct a full new triangle polygon.
   * The polygon sides should only have 3 entries.
   *
   * @param theName the polygon name. It cannot be null.
   * @param theSidesLength the polygon sides length. It cannot be empty.
   */
  public Triangle(final String theName, final List<BigDecimal> theSidesLength) {
    super(theName, PolygonType.TRIANGLE, theSidesLength);
  }

  /** The polygon perimeter.
   *
   * @return the polygon perimeter. Never null.
   */
  @Override
  public BigDecimal getPerimeter() {
    BigDecimal perimeter = BigDecimal.ZERO;
    List<BigDecimal> sides = getSideLength();
    for (int i = 0; i < this.getType().getSides(); i++){
      perimeter = perimeter.add(sides.get(i));
    }
    return perimeter.setScale(2, BigDecimal.ROUND_FLOOR);
  }

  /** The polygon area.
   *
   * @return the polygon area. Never null.
   */
  @Override
  public BigDecimal getArea() {
    BigDecimal area;
    BigDecimal s = getPerimeter().divide(BIG_DECIMAL_2);
    BigDecimal s1 = s.subtract(getHypotenuse());
    BigDecimal s2 = s.subtract(getAdjacent());
    BigDecimal s3 = s.subtract(getOpposite());
    BigDecimal term = s.multiply(s1).multiply(s2).multiply(s3);
    area = BigDecimal.valueOf(Math.sqrt(term.doubleValue()));
    return area.setScale(2, BigDecimal.ROUND_FLOOR);
  }

  /** The triangle hypotenuse side.
   * if there is no side configured, then 0 will be returned.
   *
   * @return the hypotenuse side, Never null.
   */
  public BigDecimal getHypotenuse() {
    BigDecimal width = BigDecimal.ZERO;
    List<BigDecimal> sideLength = this.getSideLength();
    if (!sideLength.isEmpty()) {
      width = sideLength.get(0);
    }
    return width;
  }

  /** The triangle adjacent side.
   * if there is no side configured, then 0 will be returned.
   *
   * @return the adjacent side, Never null.
   */
  public BigDecimal getAdjacent() {
    BigDecimal height = BigDecimal.ZERO;
    List<BigDecimal> sideLength = this.getSideLength();
    if (!sideLength.isEmpty() && sideLength.size() >= ADJACTENT_SIDE) {
      height = sideLength.get(1);
    }
    return height;
  }

  /** The triangle opposite side.
   * if there is no side configured, then 0 will be returned.
   *
   * @return the opposite side, Never null.
   */
  public BigDecimal getOpposite() {
    BigDecimal height = BigDecimal.ZERO;
    List<BigDecimal> sideLength = this.getSideLength();
    if (!sideLength.isEmpty() && sideLength.size() >= OPPOSITE_SIDE) {
      height = sideLength.get(2);
    }
    return height;
  }

  /**
   * The polygon width.
   *
   * @return the polygon width. It may be null.
   */
  @Override
  public BigDecimal getWidth() {
    return getHypotenuse();
  }

  /**
   * The polygon height.
   *
   * @return the polygon height. It may be null.
   */
  @Override
  public BigDecimal getHeight() {
    BigDecimal area = getArea();
    BigDecimal width = getWidth();
    BigDecimal height = area.multiply(BIG_DECIMAL_2).divide(width);
    return height;
  }

  /**
   * The polygon radius.
   *
   * @return the polygon radius. It may be null.
   */
  @Override
  public BigDecimal getRadius() {
    return null;
  }

  /** Verify the values are valid and the shape can be constructed.
   *
   */
  public void verifyValues() {
    BigDecimal s = getPerimeter().divide(BIG_DECIMAL_2);
    BigDecimal s1 = s.subtract(getHypotenuse());
    BigDecimal s2 = s.subtract(getAdjacent());
    BigDecimal s3 = s.subtract(getOpposite());
    Boolean isValid = ((s1.compareTo(BigDecimal.ZERO) > 0)
      && (s2.compareTo(BigDecimal.ZERO) > 0)
      && (s3.compareTo(BigDecimal.ZERO) > 0));
    Validate.isTrue(isValid,
      "The values of the triangle are not valid");
  }
}
