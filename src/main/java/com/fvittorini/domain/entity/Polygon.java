package com.fvittorini.domain.entity;

import static javax.persistence.FetchType.EAGER;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fvittorini.domain.PolygonType;
import org.apache.commons.collections.list.UnmodifiableList;
import org.apache.commons.lang3.Validate;

/**
 * Polygon domain abstract class.
 * This class is meant to represent the bases for a plane geometric shapes
 * based on its sides size.
 * @author fedevittorini
 */
@Entity
@Table(name = "polygon",
  indexes = @Index(columnList = "type", name = "IDX_POLYGON_TYPE"))
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "type")
public abstract class Polygon {

  /**
   * BigDecimal 2 value to perform divisions and multiplications.
   */
  @Transient
  public static final BigDecimal BIG_DECIMAL_2 = new BigDecimal("2");

  /**
   * Pi value as BigDecimal to perform precise operations.
   */
  @Transient
  public static final BigDecimal BIG_DECIMAL_PI = BigDecimal.valueOf(Math.PI);;

  /**
   * The user id. It cannot be null.
   */
  @Id
  @GeneratedValue
  @Column(name = "id", nullable = false)
  private Long id;

  /**
   * The polygon name. It cannot be null.
   */
  @Column(name = "name", nullable = false)
  private String name;

  /**
   * The polygon type. It cannot be null.
   */
  @Enumerated(EnumType.STRING)
  @Column(name = "type", nullable = false, insertable = false, updatable = false)
  private PolygonType type;

  /**
   * The list of polygon sides length.
   * If the sides amount is -1, the length will represent the radius.
   * It cannot be empty.
   */
  @ElementCollection(fetch = EAGER)
  @JoinColumn(name = "side_length")
  private List<BigDecimal> sideLength = new ArrayList<>();

  /**
   * The date when the polygon was created. It cannot be null.
   */
  @Column(name = "date_created", nullable = false)
  private LocalDate createdOn;

  /** Construct an empty polygon.
   * Just for Hibernate.
   */
  public Polygon() {}

  /** Construct a full new polygon.
   * The polygon sides should only have 2 entries.
   *
   * @param theName the polygon name. It cannot be null.
   * @param thePolygonType the polygon type. It cannot be null.
   * @param theSidesLength the polygon sides length. It cannot be empty.
   */
  public Polygon(final String theName, final PolygonType thePolygonType,
                 final List<BigDecimal> theSidesLength) {
    Validate.notNull(theName, "The name cannot be null.");
    Validate.notNull(thePolygonType, "The polygon type cannot be null.");
    Validate.notEmpty(theSidesLength, "The sides length cannot be empty.");
    Validate.isTrue(theSidesLength.size() == thePolygonType.getSides(),
      "Ths sides length should only contain " + thePolygonType.getSides()
        + " entries.");
    this.name = theName;
    this.type = thePolygonType;
    this.sideLength = theSidesLength;
    this.createdOn = LocalDate.now();
    verifyValues();
  }

  /** The polygon id.
   *  If this attribute is null, it means that the Polygon is not persisted.
   * @return the polygon id. It may be null.
   */
  public Long getId() {
    return this.id;
  }

  /** The polygon name.
   *
   * @return the polygon name. Never null.
   */
  public String getName() {
    if (this.name == null) {
      throw new RuntimeException("\"name\" attribute not found, object its not well formed");
    }
    return this.name;
  }

  /** The polygon type.
   *
   * @return the polygon type. Never null.
   */
  public PolygonType getType() {
    if (this.type == null) {
      throw new RuntimeException("\"type\" attribute not found, object its not well formed");
    }
    return this.type;
  }

  /** The polygon sides length.
   *
   * @return the polygon sides length. Never null.
   */
  public List<BigDecimal> getSideLength() {
    if (this.sideLength == null) {
      throw new RuntimeException("\"sideLength\" attribute not found, object its not well formed");
    }
    return UnmodifiableList.decorate(this.sideLength);
  }

  /** The polygon date created.
   *
   * @return the polygon date created. Never null.
   */
  public LocalDate getCreatedOn() {
    if (this.createdOn == null) {
      throw new RuntimeException("\"createdOn\" attribute not found, object its not well formed");
    }
    return this.createdOn;
  }

  /** The polygon perimeter.
   *
   * @return the polygon perimeter. Never null.
   */
  public abstract BigDecimal getPerimeter();

  /** The polygon area.
   *
   * @return the polygon area. Never null.
   */
  public abstract BigDecimal getArea();

  /** Verify the values are valid and the shape can be constructed.
   *
   */
  public abstract void verifyValues();

  /** The polygon width.
   *
   * @return the polygon width. It may be null.
   */
  public abstract BigDecimal getWidth();

  /** The polygon height.
   *
   * @return the polygon height. It may be null.
   */
  public abstract BigDecimal getHeight();

  /** The polygon radius.
   *
   * @return the polygon radius. It may be null.
   */
  public abstract BigDecimal getRadius();

  /** The polygon triangle hypotenuse side.
   *
   * @return the polygon triangle hypotenuse side. It may be null.
   */
  public abstract BigDecimal getHypotenuse();

  /** The polygon triangle adjacent side.
   *
   * @return the polygon triangle adjacent side. It may be null.
   */
  public abstract BigDecimal getAdjacent();

  /** The polygon triangle opposite side.
   *
   * @return the polygon triangle opposite side. It may be null.
   */
  public abstract BigDecimal getOpposite();

}
