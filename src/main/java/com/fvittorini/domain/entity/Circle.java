package com.fvittorini.domain.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.List;

import com.fvittorini.domain.PolygonType;
import org.apache.commons.lang3.Validate;

/**
 * Circle Polygon domain class.
 * This class is meant to represent a circle based on its radius.
 * @author fedevittorini
 */
@Entity
@DiscriminatorValue(PolygonType.VALUES.CIRCLE)
public class Circle extends Polygon {

  /** Construct an empty circle polygon.
   * Just for Hibernate.
   */
  public Circle() {}

  /** Construct a full new circle polygon.
   * The polygon sides should only have 1 entries representing the radius.
   *
   * @param theName the polygon name. It cannot be null.
   * @param theSidesLength the polygon sides length. It cannot be empty.
   */
  public Circle(final String theName, final List<BigDecimal> theSidesLength) {
    super(theName, PolygonType.CIRCLE, theSidesLength);
  }

  /** The polygon perimeter.
   *
   * @return the polygon perimeter. Never null.
   */
  @Override
  public BigDecimal getPerimeter() {
    BigDecimal perimeter;
    perimeter = BIG_DECIMAL_2.multiply(BIG_DECIMAL_PI).multiply(getRadius());
    return perimeter.setScale(2, BigDecimal.ROUND_FLOOR);
  }

  /** The polygon area.
   *
   * @return the polygon area. Never null.
   */
  @Override
  public BigDecimal getArea() {
    BigDecimal area;
    area = BIG_DECIMAL_PI.multiply(getRadius()).pow(2);
    return area.setScale(2, BigDecimal.ROUND_FLOOR);
  }

  /** The circle radius.
   * if there is no radius configured 0 will be returned.
   *
   * @return the circle radius. Never null.
   */
  public BigDecimal getRadius() {
    BigDecimal radius = BigDecimal.ZERO;
    if (!this.getSideLength().isEmpty()) {
      radius = this.getSideLength().get(0);
    }
    return radius;
  }

  /**
   * The polygon triangle hypotenuse side.
   *
   * @return the polygon triangle hypotenuse side. It may be null.
   */
  @Override
  public BigDecimal getHypotenuse() {
    return null;
  }

  /**
   * The polygon triangle adjacent side.
   *
   * @return the polygon triangle adjacent side. It may be null.
   */
  @Override
  public BigDecimal getAdjacent() {
    return null;
  }

  /**
   * The polygon triangle opposite side.
   *
   * @return the polygon triangle opposite side. It may be null.
   */
  @Override
  public BigDecimal getOpposite() {
    return null;
  }

  /**
   * The polygon width.
   *
   * @return the polygon width. It may be null.
   */
  @Override
  public BigDecimal getWidth() {
    return null;
  }

  /**
   * The polygon height.
   *
   * @return the polygon height. It may be null.
   */
  @Override
  public BigDecimal getHeight() {
    return null;
  }

  /** Verify the values are valid and the shape can be constructed.
   *
   */
  public void verifyValues() {
    Validate.isTrue(getRadius().compareTo(BigDecimal.ZERO) > 0,
      "The radius cannot be ZERO or negative");
  }


}
