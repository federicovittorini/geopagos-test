package com.fvittorini.domain;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

import com.fvittorini.domain.entity.Polygon;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Polygon repository class.
 *  This class provides access to stored polygons give capabilities to perform
 *  a persist, update, and delete.
 * @author fedevittorini
 */
public class PolygonRepository {

  /**
   * The class logger.
   */
  private final Logger logger = LoggerFactory.getLogger(PolygonRepository.class);

  /**
   * The entity manager. It cannot be null.
   */
  private EntityManager entityManager;

  /** The polygon repository constructor.
   *
   * @param theEntityManager the entity manager. It cannot be null.
   */
  public PolygonRepository(final EntityManager theEntityManager) {
    Validate.notNull(theEntityManager, "The entity manager cannot be null");
    entityManager = theEntityManager;
  }

  /** Persist a polygon.
   *
   * @param polygon the polygon to be persisted. It cannot be null.
   */
  @Transactional
  public void persist(final Polygon polygon) {
    entityManager.persist(polygon);
  }

  /** Delete a polygon.
   *
   * @param polygonId the polygon Id. It cannot be null.
   */
  @Transactional
  public void delete(final Long polygonId) {
    try {
      Polygon polygon = entityManager.find(Polygon.class, polygonId);
      entityManager.remove(polygon);
    } catch (Exception e) {
      logger.error("There was an error trying to remove polygon by id", e);
      throw new RuntimeException("There was an error trying to remove polygon by id", e);
    }
  }


  /** Retrieve a polygon by id.
   *
   * @param polygonId the polygon id. It cannot be null.
   * @return the polygon. Never null.
   */
  @Transactional
  public Polygon getById(final Long polygonId) {
    Polygon polygon;
    try {
      polygon =  entityManager.find(Polygon.class, polygonId);
    } catch (Exception e) {
      logger.error("There was an error trying to find polygon by id", e);
      throw new RuntimeException("There was an error trying to find polygon by id", e);
    }
    if (polygon == null) {
      throw new RuntimeException("Polygon not found");
    }
    return polygon;
  }

  /** List all polygon with filters.
   *
   * @param polygonType the polygon type to filter. It may be null.
   * @return the list of polygons. Never null.
   */
  @Transactional
  public List<Polygon> listWithFilters(final PolygonType polygonType) {
    List<Polygon> polygonList;

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Polygon> query = builder.createQuery(Polygon.class);
      Root<Polygon> root = query.from(Polygon.class);
      query = query.select(root);

      if (polygonType != null) {
        query.where(builder.equal(root.get("type"), polygonType));
      }

      TypedQuery<Polygon> q = entityManager.createQuery(query);
      polygonList = q.getResultList();
    } catch (Exception e) {
      logger.error("There was an error trying to find polygon by filters", e);
      throw new RuntimeException("There was an error trying to find polygon by filters", e);
    }

    return polygonList;
  }
}
