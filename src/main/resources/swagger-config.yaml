swagger: "2.0"
info:
  description: ""
  version: "1.0.0"
  title: "Application geopagos test"
  contact:
    email: "federicovittorini@gmail.com"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "geopagos.local"
basePath: "/v1"
tags:
- name: "server"
  description: "Everything about server"
- name: "geometry"
  description: "Everything about geometry"
schemes:
- "https"
- "http"
paths:
  /ping:
    get:
      tags:
      - "server"
      summary: "Perform a ping to host."
      description: "Perform a ping."
      operationId: "healthCheck"
      produces:
      - "text/plain"
      responses:
        200:
          description: "Successful operation."
          schema:
            type: "string"
        404:
          description: "Failed to perform healthCheck"
  /geometry:
    get:
      tags:
      - "geometry"
      summary: "List all polygons filtered by params"
      operationId: "listPolygons"
      parameters:
      - name: "type"
        in: "query"
        description: "Polygon type filter parameter"
        type: "string"
      produces:
      - "application/json"
      responses:
        200:
          description: "Successful operation."
          schema:
            $ref: '#/definitions/PolygonListDTO'
        404:
          description: "Failed to retrieve list"
    post:
      tags:
      - "geometry"
      summary: "Attempt to create a geometry"
      operationId: "createPolygon"
      consumes:
      - "application/json"
      parameters:
      - name: "polygon"
        in: "body"
        description: "The polygon to create"
        required: true
        schema:
          $ref: '#/definitions/PolygonCreationDTO'
      responses:
        200:
          description: "Successful operation."
          schema:
            $ref: '#/definitions/PolygonDTO'
        404:
          description: "Failed to create polygon"
  /geometry/{polygonId}:
    get:
      tags:
      - "geometry"
      summary: "Attempt to get a polygon by its id."
      operationId: "getById"
      parameters:
      - name: "polygonId"
        in: "path"
        description: "The polygon id"
        required: true
        type: "integer"
        format: "int64"
      produces:
      - "application/json"
      responses:
        200:
          description: "Successful operation."
          schema:
            $ref: '#/definitions/PolygonDTO'
        404:
          description: "Polygon not found."
    delete:
      tags:
      - "geometry"
      summary: "attempt to remove a polygon by given id."
      operationId: "deleteById"
      produces:
      - "application/json"
      parameters:
      - name: "polygonId"
        in: "path"
        description: "The polygon id."
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "Successful operation."
        404:
          description: "Failed to delete polygon."
definitions:
  PolygonListDTO:
    type: "array"
    items:
      $ref: '#/definitions/PolygonDTO'
  PolygonCreationDTO:
    type: "object"
    properties:
      name:
        type: "string"
        description: "The polygon name."
        example: "Triangle"
      type:
        $ref: '#/definitions/PolygonTypeDTO'
      sideLength:
        type: "array"
        description: "The list of side length."
        items:
          type: "number"
        example: [10, 32, 10]
    required:
    - name
      type
      sideSizes
  PolygonDTO:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
        description: "The polygon id"
        example: 1
      name:
        type: "string"
        description: "The polygon name."
        example: "Triangle"
      type:
        $ref: '#/definitions/PolygonTypeDTO'
        description: "The polygon type."
      sideLength:
        type: "array"
        description: "The list of side length."
        items:
          type: "number"
        example: [10, 32, 10]
      perimeter:
        type: "number"
        description: "The polygon perimeter."
        example: 45.54
      area:
        type: "number"
        description: "The polygon area."
        example: 105.54
      width:
        type: "number"
        description: "The polygon rectangle or triangle width."
        example: 35
      height:
        type: "number"
        description: "The polygon rectangle or triangle height."
        example: 40
      radius:
        type: "number"
        description: "The polygon circle radius."
        example: 15
      hypotenuse:
        type: "number"
        description: "The polygon triangle hypotenuse side."
        example: 25
      adjacent:
        type: "number"
        description: "The polygon triangle adjacent side."
        example: 20
      opposite:
        type: "number"
        description: "The polygon triangle opposite side."
        example: 18
    required:
    - id
      name
      type
      sideSizes
      perimeter
      area
  PolygonTypeDTO:
    type: "string"
    enum:
      - "TRIANGLE"
      - "RECTANGLE"
      - "CIRCLE"
    default: "TRIANGLE"
    example: "TRIANGLE"
    description: "Polygon type enum."
