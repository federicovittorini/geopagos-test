package com.fvittorini.domain.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CircleTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  private Circle circle;

  @Test
  public void testBuild_emptyOK(){
    this.circle = new Circle();

    assertThat(this.circle.getId(), is(nullValue()));
    thrown.expect(RuntimeException.class);
    thrown.expectMessage("\"name\" attribute not found, object its not well formed");
    this.circle.getName();

  }

  @Test
  public void testBuild_fullOK(){
    String theName = "circle";
    List<BigDecimal> theSidesLength = new ArrayList<>();
    theSidesLength.add(BigDecimal.ONE);
    this.circle = new Circle(theName, theSidesLength);
    assertThat(this.circle.getId(), is(nullValue()));
    assertThat(this.circle.getName(), is(theName));
    assertThat(this.circle.getPerimeter(), is(new BigDecimal("6.28")));
    assertThat(this.circle.getArea(), is(new BigDecimal("9.86")));
    assertThat(this.circle.getRadius(), is(BigDecimal.ONE));
  }
}
