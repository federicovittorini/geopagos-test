package com.fvittorini.domain.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TriangleTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  private Triangle triangle;

  @Test
  public void testBuild_emptyOK(){
    this.triangle = new Triangle();
    assertThat(this.triangle.getId(), is(nullValue()));
    thrown.expect(RuntimeException.class);
    thrown.expectMessage("\"name\" attribute not found, object its not well formed");
    this.triangle.getName();
  }

  @Test
  public void testBuild_fullOK(){
    String theName = "triangle";
    BigDecimal opposite = new BigDecimal("15.22");
    BigDecimal hypotenuse = new BigDecimal("13.07");
    List<BigDecimal> theSidesLength = new ArrayList<>();
    theSidesLength.add(hypotenuse);
    theSidesLength.add(BigDecimal.TEN);
    theSidesLength.add(opposite);
    this.triangle = new Triangle(theName, theSidesLength);
    assertThat(this.triangle.getId(), is(nullValue()));
    assertThat(this.triangle.getName(), is(theName));
    assertThat(this.triangle.getPerimeter(), is(new BigDecimal("38.29")));
    assertThat(this.triangle.getArea(), is(new BigDecimal("64.61")));
    assertThat(this.triangle.getHypotenuse(), is(hypotenuse));
    assertThat(this.triangle.getAdjacent(), is(BigDecimal.TEN));
    assertThat(this.triangle.getOpposite(), is(opposite));
  }
}
