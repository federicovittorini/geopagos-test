package com.fvittorini.domain.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RectangleTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  private Rectangle rectangle;

  @Test
  public void testBuild_emptyOK(){
    this.rectangle = new Rectangle();
    assertThat(this.rectangle.getId(), is(nullValue()));
    thrown.expect(RuntimeException.class);
    thrown.expectMessage("\"name\" attribute not found, object its not well formed");
    this.rectangle.getName();
  }

  @Test
  public void testBuild_fullOK(){
    String theName = "rectangle";
    List<BigDecimal> theSidesLength = new ArrayList<>();
    theSidesLength.add(BigDecimal.ONE);
    theSidesLength.add(BigDecimal.TEN);
    this.rectangle = new Rectangle(theName, theSidesLength);
    assertThat(this.rectangle.getId(), is(nullValue()));
    assertThat(this.rectangle.getName(), is(theName));
    assertThat(this.rectangle.getPerimeter(), is(new BigDecimal("22.00")));
    assertThat(this.rectangle.getArea(), is(new BigDecimal("10.00")));
    assertThat(this.rectangle.getWidth(), is(BigDecimal.ONE));
    assertThat(this.rectangle.getHeight(), is(BigDecimal.TEN));
  }
}
