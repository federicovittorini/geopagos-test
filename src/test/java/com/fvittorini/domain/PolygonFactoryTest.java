package com.fvittorini.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fvittorini.domain.entity.Circle;
import com.fvittorini.domain.entity.Polygon;
import com.fvittorini.domain.entity.Rectangle;
import com.fvittorini.domain.entity.Triangle;
import com.fvittorini.swagger.model.PolygonCreationDTO;
import com.fvittorini.swagger.model.PolygonDTO;
import com.fvittorini.swagger.model.PolygonTypeDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application.properties")
public class PolygonFactoryTest {

  @Test
  public void testCreateTrianglePolygon_OK() throws Exception {
    String theName = "triangle";
    PolygonTypeDTO theType = PolygonTypeDTO.TRIANGLE;
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    PolygonCreationDTO polygonCreationDTO = new PolygonCreationDTO();
    polygonCreationDTO.setName(theName);
    polygonCreationDTO.setType(theType);
    polygonCreationDTO.setSideLength(sideLength);
    Polygon result = PolygonFactory.createPolygon(polygonCreationDTO);
    assertThat(result, is(instanceOf(Triangle.class)));
  }

  @Test
  public void testCreateCirclePolygon_OK() throws Exception {
    String theName = "cicle";
    PolygonTypeDTO theType = PolygonTypeDTO.CIRCLE;
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.ONE);
    PolygonCreationDTO polygonCreationDTO = new PolygonCreationDTO();
    polygonCreationDTO.setName(theName);
    polygonCreationDTO.setType(theType);
    polygonCreationDTO.setSideLength(sideLength);
    Polygon result = PolygonFactory.createPolygon(polygonCreationDTO);
    assertThat(result, is(instanceOf(Circle.class)));
  }

  @Test
  public void testCreateRectanglePolygon_OK() throws Exception {
    String theName = "rectangle";
    PolygonTypeDTO theType = PolygonTypeDTO.RECTANGLE;
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    PolygonCreationDTO polygonCreationDTO = new PolygonCreationDTO();
    polygonCreationDTO.setName(theName);
    polygonCreationDTO.setType(theType);
    polygonCreationDTO.setSideLength(sideLength);
    Polygon result = PolygonFactory.createPolygon(polygonCreationDTO);
    assertThat(result, is(instanceOf(Rectangle.class)));
  }

  @Test
  public void testFillCircleFullPolygonDTO() throws Exception {
    String theName = "circle";
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    Circle circle = new Circle(theName, sideLength);
    PolygonDTO result = PolygonFactory.fillFullPolygonDTO(circle);
    assertThat(result.getName(), is(theName));
    assertThat(result.getType(), is(PolygonTypeDTO.CIRCLE));
  }

  @Test
  public void testFillTriangleFullPolygonDTO() throws Exception {
    String theName = "circle";
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    Triangle triangle = new Triangle(theName, sideLength);
    PolygonDTO result = PolygonFactory.fillFullPolygonDTO(triangle);
    assertThat(result.getName(), is(theName));
    assertThat(result.getType(), is(PolygonTypeDTO.TRIANGLE));
  }

  @Test
  public void testFillRectangleFullPolygonDTO() throws Exception {
    String theName = "rectangle";
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    Rectangle triangle = new Rectangle(theName, sideLength);
    PolygonDTO result = PolygonFactory.fillFullPolygonDTO(triangle);
    assertThat(result.getName(), is(theName));
    assertThat(result.getType(), is(PolygonTypeDTO.RECTANGLE));
  }
}