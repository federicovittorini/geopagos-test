package com.fvittorini.domain;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.fvittorini.swagger.model.PolygonTypeDTO;
import org.junit.Test;

public class PolygonTypeTest {

  @Test
  public void testPolygonType(){
    assertThat(PolygonType.CIRCLE.getName(), is(PolygonType.VALUES.CIRCLE));
    assertThat(PolygonType.CIRCLE.getSides(), is(not(nullValue())));
    assertThat(PolygonType.RECTANGLE.getName(), is(PolygonType.VALUES.RECTANGLE));
    assertThat(PolygonType.RECTANGLE.getSides(), is(not(nullValue())));
    assertThat(PolygonType.TRIANGLE.getName(), is(PolygonType.VALUES.TRIANGLE));
    assertThat(PolygonType.TRIANGLE.getSides(), is(not(nullValue())));

    assertThat(PolygonType.VALUES.TRIANGLE, is(PolygonTypeDTO.TRIANGLE.name()));
    assertThat(PolygonType.VALUES.RECTANGLE, is(PolygonTypeDTO.RECTANGLE.name()));
    assertThat(PolygonType.VALUES.CIRCLE, is(PolygonTypeDTO.CIRCLE.name()));
  }
}
