package com.fvittorini.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fvittorini.domain.PolygonRepository;
import com.fvittorini.domain.entity.Polygon;
import com.fvittorini.domain.entity.Triangle;
import com.fvittorini.swagger.model.PolygonCreationDTO;
import com.fvittorini.swagger.model.PolygonDTO;
import com.fvittorini.swagger.model.PolygonTypeDTO;
import org.junit.Before;
import org.junit.Test;

public class PolygonServiceTest {

  private PolygonRepository polygonRepository;

  private PolygonService polygonService;

  @Before
  public void setUp() {
    polygonRepository = mock(PolygonRepository.class);
    polygonService = new PolygonService(polygonRepository);
  }

  @Test
  public void testCreatePolygon() throws Exception {
    String polygonName = "circle";
    PolygonTypeDTO polygonTypeDTO = PolygonTypeDTO.CIRCLE;
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    PolygonCreationDTO polygonCreationDTO = new PolygonCreationDTO();
    polygonCreationDTO.setName(polygonName);
    polygonCreationDTO.setType(polygonTypeDTO);
    polygonCreationDTO.setSideLength(sideLength);
    PolygonDTO result = polygonService.createPolygon(polygonCreationDTO);

    assertThat(result.getName(), equalTo(polygonCreationDTO.getName()));
    assertThat(result.getType(), equalTo(polygonCreationDTO.getType()));
    assertThat(result.getSideLength().size(),
      equalTo(polygonCreationDTO.getSideLength().size()));
  }

  @Test
  public void testGetPolygonById() throws Exception {
    Long polygonId = 1L;
    String polygonName = "triangle";
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    Polygon polygon = new Triangle(polygonName, sideLength);

    when(polygonRepository.getById(polygonId)).thenReturn(polygon);

    PolygonDTO result = polygonService.getPolygonById(polygonId);

    assertThat(result.getName(), equalTo(polygonName));
    assertThat(result.getType(), equalTo(PolygonTypeDTO.TRIANGLE));
    assertThat(result.getSideLength().size(),
      equalTo(sideLength.size()));
  }

  @Test
  public void testListPolygons() throws Exception {
    String polygonName = "triangle";
    List<BigDecimal> sideLength = new ArrayList<>();
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    sideLength.add(BigDecimal.TEN);
    Polygon polygon = new Triangle(polygonName, sideLength);
    Polygon polygon2 = new Triangle(polygonName, sideLength);
    Polygon polygon3 = new Triangle(polygonName, sideLength);

    List<Polygon> polygonList = new ArrayList<>();
    polygonList.add(polygon);
    polygonList.add(polygon2);
    polygonList.add(polygon3);

    when(polygonRepository.listWithFilters(any())).thenReturn(polygonList);

    List<PolygonDTO> resultList = polygonService.listPolygons(null);
    assertThat(resultList.size(), equalTo(polygonList.size()));
    PolygonDTO result = resultList.get(0);

    assertThat(result.getName(), equalTo(polygonName));
    assertThat(result.getType(), equalTo(PolygonTypeDTO.TRIANGLE));
    assertThat(result.getSideLength().size(),
      equalTo(sideLength.size()));
  }
}